package com.demo.weatherdemo.repository

import com.demo.weatherdemo.network.dto.WeatherResponseDto
import com.demo.weatherdemo.network.retrofit.CustomResult

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

interface IWeatherRepository {

    suspend fun getOneCallWeatherData(coordinates: Map<String, Double>): CustomResult<WeatherResponseDto>
}