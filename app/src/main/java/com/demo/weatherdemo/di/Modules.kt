package com.demo.weatherdemo.di

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import com.demo.weatherdemo.ui.LocationViewModel
import com.demo.weatherdemo.network.api.IWeatherApi
import com.demo.weatherdemo.network.connectivity.PostMNetworkChecker
import com.demo.weatherdemo.network.connectivity.PreMNetworkChecker
import com.demo.weatherdemo.network.okhttp.DefaultOkHttpClientProvider
import com.demo.weatherdemo.network.okhttp.IOkHttpClientProvider
import com.demo.weatherdemo.network.retrofit.RetrofitBuilderImpl
import com.demo.weatherdemo.repository.IWeatherRepository
import com.demo.weatherdemo.repository.WeatherRepositoryImpl
import com.demo.weatherdemo.ui.WeatherViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

val applicationModule = module {
    single { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    single<IWeatherRepository> { WeatherRepositoryImpl(get()) }
}

val viewModelModule = module {
    viewModel { WeatherViewModel(get()) }
    single { LocationViewModel(get()) }
}

val networkModule = module {
    single<IOkHttpClientProvider> {
        DefaultOkHttpClientProvider(get())
    }

    single<IWeatherApi> {
        RetrofitBuilderImpl(DefaultOkHttpClientProvider(get()).get()).build()
            .create(IWeatherApi::class.java)
    }

    single {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            PreMNetworkChecker(get())
        } else {
            PostMNetworkChecker(get())
        }
    }

    single {
        WeatherRepositoryImpl(get())
    }
}