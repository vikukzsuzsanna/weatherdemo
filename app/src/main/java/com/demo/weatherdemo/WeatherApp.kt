package com.demo.weatherdemo

import android.app.Application
import com.demo.weatherdemo.di.applicationModule
import com.demo.weatherdemo.di.networkModule
import com.demo.weatherdemo.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

class WeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherApp)
            modules(
                networkModule,
                applicationModule,
                viewModelModule
            )
        }
        setUpCrashRepostring()
    }

    private fun setUpCrashRepostring() {
        Timber.plant(Timber.DebugTree())
    }
}