package com.demo.weatherdemo

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.demo.weatherdemo.common.showDialogWithFunction
import com.demo.weatherdemo.ui.LocationViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    companion object {
        const val LAT_PARAM = "lat"
        const val LON_PARAM = "lon"
    }

    private lateinit var navController: NavController

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val locationViewModel: LocationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navController = Navigation.findNavController(this, R.id.navHostFragment)
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        subscribeOnLocationPermissionObservable()
        locationViewModel.checkLocation()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationViewModel.checkLocation()
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        )
                    ) {
                        showDialogWithFunction(
                            this,
                            getString(R.string.warning),
                            getString(R.string.need_location_permission),
                            getString(R.string.ok)
                        ) {
                            requestLocationPermission()
                        }
                    } else {
                        locationViewModel.locationPermissionDeniedObservable.postValue(Unit)
                    }
                }
            }
        }
    }

    private fun subscribeOnLocationPermissionObservable() {
        locationViewModel.locationRequestPermissionObservable.observe(this, {
            requestLocationPermission()
        })
    }

    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            1
        )
    }
}
