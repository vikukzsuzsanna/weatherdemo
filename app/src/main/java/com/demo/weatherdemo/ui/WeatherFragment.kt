package com.demo.weatherdemo.ui

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import com.demo.weatherdemo.R
import com.demo.weatherdemo.common.showErrorDialog
import com.demo.weatherdemo.domain.WeatherData
import kotlinx.android.synthetic.main.fragment_weather.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherFragment : Fragment() {

    private val weatherViewModel: WeatherViewModel by viewModel()
    private val locationViewModel: LocationViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeOnLocationObservables()
        subscribeOnWeatherObservables()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when (item.itemId) {
            R.id.menu_refresh -> {
                locationViewModel.checkLocation()
            }
        }
        return true
    }

    private fun subscribeOnLocationObservables() {
        locationViewModel.locationSuccessObservable.observe(viewLifecycleOwner, {
            weatherViewModel.getWeatherData(it)
        })
        locationViewModel.locationLoadingObservable.observe(viewLifecycleOwner, {
            if (it) {
                showProgress()
                progressText.text = getString(R.string.fetching_location_progress_text)
            }
        })
        locationViewModel.locationErrorObservable.observe(viewLifecycleOwner, {
            contentScrollView.visibility = GONE
            progressContainer.visibility = GONE
            showErrorDialog(
                requireContext(),
                getString(R.string.error),
                getString(R.string.no_location)
            )
        })
        locationViewModel.locationPermissionDeniedObservable.observe(viewLifecycleOwner, {
            showLocationDeniedText()
        })
        locationViewModel.locationGpsDisabledObservable.observe(viewLifecycleOwner, {
            showGpsDisabledText()
        })
    }

    private fun subscribeOnWeatherObservables() {
        weatherViewModel.weatherObservable.observe(viewLifecycleOwner, {
            setWeatherData(it)
        })
        weatherViewModel.errorObservable.observe(viewLifecycleOwner, {
            showErrorDialog(
                requireContext(),
                getString(R.string.error),
                getString(R.string.weather_unavailable)
            )
        })
        weatherViewModel.noInternetObservable.observe(viewLifecycleOwner, {
            showErrorDialog(
                requireContext(),
                getString(R.string.error),
                getString(R.string.no_internet)
            )
        })
        weatherViewModel.loadingObservable.observe(viewLifecycleOwner, {
            if (it) {
                showProgress()
                progressText.text = getString(R.string.fetching_weather_progress_text)
            } else hideProgress()
        })
    }

    private fun showLocationDeniedText() {
        contentScrollView.visibility = GONE
        progressContainer.visibility = GONE
        missingPermissionText.visibility = VISIBLE
        missingPermissionText.text = getString(R.string.no_location)
    }

    private fun showGpsDisabledText() {
        contentScrollView.visibility = GONE
        progressContainer.visibility = GONE
        missingPermissionText.visibility = VISIBLE
        missingPermissionText.text = getString(R.string.enable_gps)
    }

    private fun setWeatherData(data: WeatherData) {
        data.apply {
            locationNameText.setText(timezone)
            current?.apply {
                currentDateText.setText(date)
                weatherDescriptionText.setText(weather?.joinToString(", ") { "${it.description}" })
                sunriseText.setText(sunrise)
                sunsetText.setText(sunset)
                currentTemperatureText.setText(temperature.toString())
                temperatureFeelsLikeText.setText(feelsLike.toString())
                pressureText.setText(pressure.toString())
                humidityText.setText("${humidity.toString()} %")
                uvIndexText.setText(uvIndex.toString())
                cloudCoverageText.setText("${clouds.toString()} %")
                visibilityText.setText("${visibility.toString()} m")
                windSpeedText.setText("${windSpeed.toString()} km/h")

                val dailyAdapter = DailyAdapter()
                daily?.apply { dailyAdapter.dailyList = this }
                dailyRecView.adapter = dailyAdapter
                dailyRecView.layoutManager =
                    LinearLayoutManager(requireContext(), HORIZONTAL, false)
                dailyRecView.addItemDecoration(
                    DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL)
                )
            }
        }
    }

    private fun showProgress() {
        missingPermissionText.visibility = GONE
        contentScrollView.visibility = GONE
        progressContainer.visibility = VISIBLE
    }

    private fun hideProgress() {
        contentScrollView.visibility = VISIBLE
        progressContainer.visibility = GONE
    }
}
