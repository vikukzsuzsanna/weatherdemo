package com.demo.weatherdemo.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.weatherdemo.domain.WeatherData
import com.demo.weatherdemo.network.retrofit.CustomResult
import com.demo.weatherdemo.repository.IWeatherRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class WeatherViewModel(
    private val repository: IWeatherRepository
) : ViewModel() {

    val weatherObservable: MutableLiveData<WeatherData> = MutableLiveData()
    val errorObservable: MutableLiveData<String?> = MutableLiveData()
    val noInternetObservable: MutableLiveData<Unit> = MutableLiveData()
    val loadingObservable: MutableLiveData<Boolean> = MutableLiveData()

    fun getWeatherData(coordinates: Map<String, Double>) {
        loadingObservable.postValue(true)
        viewModelScope.launch(IO) {
            repository.getOneCallWeatherData(coordinates).run {
                when (status) {
                    CustomResult.Status.SUCCESS ->
                        weatherObservable.postValue(data?.toModel())
                    CustomResult.Status.API_ERROR -> errorObservable.postValue(message)
                    CustomResult.Status.NETWORK_ERROR -> noInternetObservable.postValue(Unit)
                    CustomResult.Status.UNKNOWN_ERROR -> errorObservable.postValue(message)
                }
                loadingObservable.postValue(false)
            }
        }
    }
}