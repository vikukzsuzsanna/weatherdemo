package com.demo.weatherdemo.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.demo.weatherdemo.R
import com.demo.weatherdemo.common.notifyChanges
import com.demo.weatherdemo.domain.Daily
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_daily.view.*
import kotlin.properties.Delegates

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 2/1/2021.
 */

class DailyAdapter : RecyclerView.Adapter<DailyAdapter.ViewHolder>() {

    var dailyList: List<Daily> by Delegates.observable(emptyList()) { _, old, new ->
        notifyChanges(old, new) { o, n -> o.date == n.date }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_daily, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (dailyList.isNotEmpty()) {
            dailyList[position].apply {
                holder.bind(this)
            }
        }
    }

    override fun getItemCount(): Int {
        return dailyList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
        override val containerView: View
            get() = itemView

        fun bind(item: Daily) {
            containerView.apply {
                dailyDate.text = item.date
                dailyWeatherDescription.text =
                    item.weather?.joinToString(", ") { "${it.description}" }
                dailyPopText.text = "${item.pop} %"
                dailyMaxText.text = "${item.temperature?.max} °C"
                dailyMinText.text = "${item.temperature?.min} °C"
            }
        }
    }
}