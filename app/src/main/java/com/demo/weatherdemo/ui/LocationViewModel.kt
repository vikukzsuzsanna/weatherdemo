package com.demo.weatherdemo.ui

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.weatherdemo.MainActivity
import com.demo.weatherdemo.common.hasLocationPermission
import com.demo.weatherdemo.common.isGpsEnabled
import com.google.android.gms.location.FusedLocationProviderClient


/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/31/2021.
 */

class LocationViewModel(
    private val context: Context
) : ViewModel() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    val locationSuccessObservable: MutableLiveData<Map<String, Double>> = MutableLiveData()
    val locationGpsDisabledObservable: MutableLiveData<Unit> = MutableLiveData()
    val locationErrorObservable: MutableLiveData<Unit> = MutableLiveData()
    val locationPermissionDeniedObservable: MutableLiveData<Unit> = MutableLiveData()
    val locationLoadingObservable: MutableLiveData<Boolean> = MutableLiveData()

    val locationRequestPermissionObservable: MutableLiveData<Unit> = MutableLiveData()

    fun checkLocation() {
        if (isGpsEnabled(context)) {
            if (hasLocationPermission(context)) {
                provideLocation()
            } else {
                locationRequestPermissionObservable.postValue(Unit)
            }
        } else {
            locationGpsDisabledObservable.postValue(Unit)
        }
    }

    @SuppressLint("MissingPermission", "VisibleForTests")
    private fun provideLocation() {
        locationLoadingObservable.postValue(true)
        fusedLocationClient = FusedLocationProviderClient(context)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    locationSuccessObservable.postValue(
                        mapOf(
                            MainActivity.LAT_PARAM to location.latitude,
                            MainActivity.LON_PARAM to location.longitude
                        )
                    )
                }
                else {
                    locationErrorObservable.postValue(Unit)
                }
                locationLoadingObservable.postValue(false)
            }.addOnFailureListener {
                locationLoadingObservable.postValue(false)
                locationErrorObservable.postValue(Unit)
            }
    }
}
