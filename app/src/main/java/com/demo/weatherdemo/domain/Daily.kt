package com.demo.weatherdemo.domain

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

class Daily(
    val date: String?,
    val sunrise: String?,
    val sunset: String?,
    val temperature: Temperature?,
    val feelsLike: Temperature?,
    val pressure: Long?,
    val humidity: Int?,
    val dewPoint: Double?,
    val windSpeed: Double?,
    val windDegree: Int?,
    val weather: List<Weather>?,
    val clouds: Int?,
    val pop: Double?,
    val rain: Double?,
    val snow: Double?,
    val uvIndex: Double?
)