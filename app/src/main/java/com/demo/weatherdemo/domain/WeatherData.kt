package com.demo.weatherdemo.domain

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

class WeatherData(
    val lat: Double?,
    val lon: Double?,
    val timezone: String?,
    val timezoneOffset: Long?,
    val current: CurrentWeather?,
    val daily: List<Daily>?,
    val clouds: Int?,
    val alerts: List<Alert>?
)