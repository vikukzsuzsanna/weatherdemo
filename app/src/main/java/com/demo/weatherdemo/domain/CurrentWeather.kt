package com.demo.weatherdemo.domain

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

class CurrentWeather(
    val date: String?,
    val sunrise: String?,
    val sunset: String?,
    val temperature: Double?,
    val feelsLike: Double?,
    val pressure: Long?,
    val humidity: Int?,
    val dewPoint: Double?,
    val uvIndex: Double?,
    val clouds: Int?,
    val rain: Double?,
    val snow: Double?,
    val visibility: Long?,
    val windSpeed: Double?,
    val windDegree: Int?,
    val weather: List<Weather>?
)