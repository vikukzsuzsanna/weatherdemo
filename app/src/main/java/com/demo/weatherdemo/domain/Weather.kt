package com.demo.weatherdemo.domain

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

class Weather(
    val id: Long?,
    val main: String?,
    val description: String?,
    val icon: String?
)