package com.demo.weatherdemo.domain

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

class Alert(
    val senderName: String?,
    val event: String?,
    val start: String?,
    val end: String?,
    val description: String?,
)