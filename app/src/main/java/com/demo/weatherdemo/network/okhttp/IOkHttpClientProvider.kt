package com.demo.weatherdemo.network.okhttp

import okhttp3.OkHttpClient

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

interface IOkHttpClientProvider {

    fun get(): OkHttpClient
}
