package com.demo.weatherdemo.network.api

import com.demo.weatherdemo.network.dto.WeatherResponseDto
import com.demo.weatherdemo.network.retrofit.CustomResult
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

interface IWeatherApi {

    companion object {
        const val API_KEY = "db5c0c36cb360febaac3506852a6796b"
        const val APP_ID_PARAM = "appid"
        const val EXCLUDE_PARAM = "exclude"
        const val EXCLUDE_VALUES = "minutely,hourly"
        const val UNITS_PARAM = "units"
        const val UNITS_VALUE = "metric"
    }

    @GET("onecall?$APP_ID_PARAM=$API_KEY&$EXCLUDE_PARAM=$EXCLUDE_VALUES&$UNITS_PARAM=$UNITS_VALUE")
    suspend fun getOneCallWeatherData(@QueryMap coordinatesMap: Map<String, Double>): CustomResult<WeatherResponseDto>
}