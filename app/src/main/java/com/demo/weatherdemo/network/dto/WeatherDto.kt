package com.demo.weatherdemo.network.dto

import com.demo.weatherdemo.domain.Weather
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

@JsonClass(generateAdapter = true)
class WeatherDto(
    @field:Json(name = "id")
    val id: Long?,
    @field:Json(name = "main")
    val main: String?,
    @field:Json(name = "description")
    val description: String?,
    @field:Json(name = "icon")
    val icon: String?
) {
    fun toModel() = Weather(
        id,
        main,
        description,
        icon
    )
}