package com.demo.weatherdemo.network.retrofit

import com.demo.weatherdemo.network.dto.ResponseWrapper
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

class NetworkResponseCall<S : ResponseWrapper>(
    private val delegate: Call<S>
) : Call<CustomResult<S>> {

    override fun enqueue(callback: Callback<CustomResult<S>>) {
        return delegate.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                val body = response.body()

                if (response.isSuccessful) {
                    if (body != null) {
                        if (body.errorMessage == null && body.errorCode == null) {
                            callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(CustomResult.success(body))
                            )
                        } else {
                            callback.onResponse(
                                this@NetworkResponseCall,
                                Response.success(CustomResult.apiError(body.errorMessage))
                            )
                        }
                    } else {
                        callback.onResponse(
                            this@NetworkResponseCall,
                            Response.success(CustomResult.unknownError(null))
                        )
                    }
                } else {
                    callback.onResponse(
                        this@NetworkResponseCall,
                        Response.success(CustomResult.unknownError(null))
                    )
                }
            }

            override fun onFailure(call: Call<S>, t: Throwable) {
                val networkResponse = when (t) {
                    is IOException -> CustomResult.networkError(null)
                    else -> CustomResult.unknownError(null)
                }
                callback.onResponse(
                    this@NetworkResponseCall,
                    Response.success(networkResponse)
                )
            }
        })
    }

    override fun isExecuted(): Boolean = delegate.isExecuted

    override fun clone(): Call<CustomResult<S>> =
        NetworkResponseCall(delegate.clone())

    override fun isCanceled(): Boolean = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<CustomResult<S>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()
}