package com.demo.weatherdemo.network.dto

import com.demo.weatherdemo.common.DATE_FORMAT
import com.demo.weatherdemo.common.TIME_FORMAT
import com.demo.weatherdemo.common.toFormattedString
import com.demo.weatherdemo.domain.CurrentWeather
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

@JsonClass(generateAdapter = true)
class CurrentWeatherDto(
    @field:Json(name = "dt")
    val date: Long?,
    @field:Json(name = "sunrise")
    val sunrise: Long?,
    @field:Json(name = "sunset")
    val sunset: Long?,
    @field:Json(name = "temp")
    val temperature: Double?,
    @field:Json(name = "feels_like")
    val feelsLike: Double?,
    @field:Json(name = "pressure")
    val pressure: Long?,
    @field:Json(name = "humidity")
    val humidity: Int?,
    @field:Json(name = "dew_point")
    val dewPoint: Double?,
    @field:Json(name = "uvi")
    val uvIndex: Double?,
    @field:Json(name = "clouds")
    val clouds: Int?,
    @field:Json(name = "rain")
    val rain: Double?,
    @field:Json(name = "snow")
    val snow: Double?,
    @field:Json(name = "visibility")
    val visibility: Long?,
    @field:Json(name = "wind_speed")
    val windSpeed: Double?,
    @field:Json(name = "wind_deg")
    val windDegree: Int?,
    @field:Json(name = "weather")
    val weather: List<WeatherDto>?
) {
    fun toModel() = CurrentWeather(
        date?.toFormattedString(DATE_FORMAT),
        sunrise?.toFormattedString(TIME_FORMAT),
        sunset?.toFormattedString(TIME_FORMAT),
        temperature,
        feelsLike,
        pressure,
        humidity,
        dewPoint,
        uvIndex,
        clouds,
        rain,
        snow,
        visibility,
        windSpeed,
        windDegree,
        weather?.map { it.toModel() }
    )
}