package com.demo.weatherdemo.network.okhttp

import com.demo.weatherdemo.network.connectivity.INetworkChecker
import com.demo.weatherdemo.network.connectivity.NoInternetConnectionException
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

open class BaseOkHttpClientProvider {

    protected fun getLoggerInterceptor(): Interceptor =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    protected fun getNetworkConnectionInterceptor(networkChecker: INetworkChecker): Interceptor =
        Interceptor { chain ->
            if (!networkChecker.hasNetworkConnectivity()) throw NoInternetConnectionException()

            val builder = chain.request().newBuilder()
            chain.proceed(builder.build())
        }

}