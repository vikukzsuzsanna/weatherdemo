package com.demo.weatherdemo.network.dto

import com.demo.weatherdemo.common.DATE_TIME_FORMAT
import com.demo.weatherdemo.common.toFormattedString
import com.demo.weatherdemo.domain.Alert
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

@JsonClass(generateAdapter = true)
class AlertDto(
    @field:Json(name = "sender_name")
    val senderName: String?,
    @field:Json(name = "event")
    val event: String?,
    @field:Json(name = "start")
    val start: Long?,
    @field:Json(name = "end")
    val end: Long?,
    @field:Json(name = "description")
    val description: String?,
) {
    fun toModel() = Alert(
        senderName,
        event,
        start?.toFormattedString(DATE_TIME_FORMAT),
        end?.toFormattedString(DATE_TIME_FORMAT),
        description
    )
}