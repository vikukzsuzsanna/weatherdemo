package com.demo.weatherdemo.network.okhttp

import com.demo.weatherdemo.network.connectivity.INetworkChecker
import okhttp3.OkHttpClient

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

class DefaultOkHttpClientProvider(
    private val networkChecker: INetworkChecker
) : BaseOkHttpClientProvider(), IOkHttpClientProvider {
    override fun get(): OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(getNetworkConnectionInterceptor(networkChecker))
            .addInterceptor(getLoggerInterceptor())
            .build()
    }
}
