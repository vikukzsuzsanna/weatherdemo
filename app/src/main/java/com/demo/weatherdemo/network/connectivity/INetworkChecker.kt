package com.demo.weatherdemo.network.connectivity

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

interface INetworkChecker {
    fun hasNetworkConnectivity(): Boolean
}