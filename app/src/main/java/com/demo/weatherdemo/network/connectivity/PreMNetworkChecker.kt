package com.demo.weatherdemo.network.connectivity

import android.net.ConnectivityManager

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

class PreMNetworkChecker(
    private val connectivityManager: ConnectivityManager
) : INetworkChecker {
    @Suppress("DEPRECATION")
    override fun hasNetworkConnectivity(): Boolean {
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }

}