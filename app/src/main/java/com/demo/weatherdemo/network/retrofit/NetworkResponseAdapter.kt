package com.demo.weatherdemo.network.retrofit

import com.demo.weatherdemo.network.dto.ResponseWrapper
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

class NetworkResponseAdapter<S : ResponseWrapper>(
    private val successType: Type
) : CallAdapter<S, Call<CustomResult<S>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<S>): Call<CustomResult<S>> {
        return NetworkResponseCall(call)
    }
}