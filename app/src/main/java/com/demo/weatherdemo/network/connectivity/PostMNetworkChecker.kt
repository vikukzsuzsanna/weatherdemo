package com.demo.weatherdemo.network.connectivity

import android.annotation.TargetApi
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

class PostMNetworkChecker(
    private val connectivityManager: ConnectivityManager
) : INetworkChecker {

    @TargetApi(Build.VERSION_CODES.M)
    override fun hasNetworkConnectivity(): Boolean {

        val network = connectivityManager.activeNetwork
        val connection =
            connectivityManager.getNetworkCapabilities(network)

        return connection != null && (
                connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                        connection.hasTransport(NetworkCapabilities.TRANSPORT_VPN))
    }


}