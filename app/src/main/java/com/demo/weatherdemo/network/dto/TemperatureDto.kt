package com.demo.weatherdemo.network.dto

import com.demo.weatherdemo.domain.Temperature
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

@JsonClass(generateAdapter = true)
class TemperatureDto(
    @field:Json(name = "day")
    val day: Double?,
    @field:Json(name = "min")
    val min: Double?,
    @field:Json(name = "max")
    val max: Double?,
    @field:Json(name = "night")
    val night: Double?,
    @field:Json(name = "eve")
    val eve: Double?,
    @field:Json(name = "morn")
    val morn: Double?
) {
    fun toModel() = Temperature(
        day,
        min,
        max,
        night,
        eve,
        morn
    )
}