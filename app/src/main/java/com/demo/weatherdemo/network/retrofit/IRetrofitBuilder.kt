package com.demo.weatherdemo.network.retrofit

import retrofit2.Retrofit

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

interface IRetrofitBuilder {

    fun build(): Retrofit
}