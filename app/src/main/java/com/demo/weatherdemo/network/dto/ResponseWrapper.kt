package com.demo.weatherdemo.network.dto

import com.squareup.moshi.Json

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

abstract class ResponseWrapper(
    @property:Json(name = "cod")
    var errorCode: String? = null,
    @property:Json(name = "message")
    var errorMessage: String? = null
)