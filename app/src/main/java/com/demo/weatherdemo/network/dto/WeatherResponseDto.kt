package com.demo.weatherdemo.network.dto

import com.demo.weatherdemo.domain.WeatherData
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

@JsonClass(generateAdapter = true)
class WeatherResponseDto(
    @field:Json(name = "lat")
    val lat: Double?,
    @field:Json(name = "lon")
    val lon: Double?,
    @field:Json(name = "timezone")
    val timezone: String?,
    @field:Json(name = "timezone_offset")
    val timezoneOffset: Long?,
    @field:Json(name = "current")
    val current: CurrentWeatherDto?,
    @field:Json(name = "daily")
    val daily: List<DailyDto>?,
    @field:Json(name = "clouds")
    val clouds: Int?,
    @field:Json(name = "alerts")
    val alerts: List<AlertDto>?
) : ResponseWrapper() {
    fun toModel() = WeatherData(
        lat,
        lon,
        timezone,
        timezoneOffset,
        current?.toModel(),
        daily?.map { it.toModel() },
        clouds,
        alerts?.map { it.toModel() }
    )
}