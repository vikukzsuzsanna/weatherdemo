package com.demo.weatherdemo.network.connectivity

import java.io.IOException

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/28/2021.
 */

class NoInternetConnectionException : IOException()