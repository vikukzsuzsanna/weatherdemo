package com.demo.weatherdemo.network.retrofit

import com.demo.weatherdemo.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

class RetrofitBuilderImpl(
    private val okHttpClient: OkHttpClient
) : IRetrofitBuilder {

    override fun build(): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(BuildConfig.WEATHER_API_BASE_URL)
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .addConverterFactory(
                MoshiConverterFactory
                    .create(
                        Moshi
                            .Builder()
                            .add(KotlinJsonAdapterFactory())
                            .build()
                    ).asLenient()
            )
            .client(okHttpClient)
            .build()
    }
}