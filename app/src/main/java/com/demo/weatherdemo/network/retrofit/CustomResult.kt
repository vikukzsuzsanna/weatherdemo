package com.demo.weatherdemo.network.retrofit

import com.demo.weatherdemo.network.dto.ResponseWrapper

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/29/2021.
 */

class CustomResult<out T : ResponseWrapper>(
    val status: Status?,
    val data: T?,
    val message: String?
) {

    enum class Status {
        SUCCESS,
        API_ERROR,
        NETWORK_ERROR,
        UNKNOWN_ERROR
    }

    companion object {
        fun <T : ResponseWrapper> success(data: T): CustomResult<T> {
            return CustomResult(Status.SUCCESS, data, null)
        }

        fun apiError(errorMessage: String?): CustomResult<Nothing> {
            return CustomResult(Status.API_ERROR, null, errorMessage)
        }

        fun networkError(errorMessage: String?): CustomResult<Nothing> {
            return CustomResult(Status.NETWORK_ERROR, null, errorMessage)
        }

        fun unknownError(errorMessage: String?): CustomResult<Nothing> {
            return CustomResult(Status.UNKNOWN_ERROR, null, errorMessage)
        }
    }
}