package com.demo.weatherdemo.common

import android.app.AlertDialog
import android.content.Context
import com.demo.weatherdemo.R

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */

const val DATE_FORMAT = "yyyy. MM. dd."
const val DATE_TIME_FORMAT = "yyyy. MM. dd. kk:mm"
const val TIME_FORMAT = "kk:mm"
const val DAY_FORMAT = "E, MMM d"

fun showDialogWithFunction(
    context: Context,
    title: String,
    message: String,
    btnText: String,
    function: () -> Unit
) {
    AlertDialog
        .Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(btnText) { _, _ ->
            function.invoke()
        }
        .show()
}

fun showErrorDialog(
    context: Context,
    title: String,
    message: String
) {
    AlertDialog
        .Builder(context)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(R.string.ok) { dialog, _ ->
            dialog.dismiss()
        }
        .show()
}
