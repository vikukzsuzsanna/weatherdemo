package com.demo.weatherdemo.common

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.demo.weatherdemo.R
import kotlinx.android.synthetic.main.layout_labeled_text.view.*

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/31/2021.
 */

class LabeledTextView @JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_labeled_text, this, true)
        orientation = VERTICAL
        attrs?.let {
            val array = context.obtainStyledAttributes(it, R.styleable.LabeledTextView, 0, 0)
            if (array.hasValue(R.styleable.LabeledTextView_labelText)) {
                labelText.text = array.getString(R.styleable.LabeledTextView_labelText)
            }
            if (array.hasValue(R.styleable.LabeledTextView_valueText)) {
                valueText.text = array.getString(R.styleable.LabeledTextView_valueText)
            }
            array.recycle()
        }
    }

    /**
     * Sets the text value for the layout.
     *
     * @param value This will be displayed.
     */
    fun setText(value: String?) {
        value?.let {
            visibility = View.VISIBLE
            valueText.text = value
        }
    }
}
