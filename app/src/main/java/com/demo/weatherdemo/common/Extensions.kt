package com.demo.weatherdemo.common

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.demo.weatherdemo.BuildConfig.*

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.*
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * @author Zsuzsanna Vikuk
 * @author www.mikrum.hu
 * Creaated on 1/30/2021.
 */


@SuppressLint("SimpleDateFormat")
fun Long.toFormattedString(format: String): String {
    return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
        SimpleDateFormat(format).format(Date(this * 1000))
    } else {
            Instant
                .ofEpochSecond(this)
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime().format(DateTimeFormatter.ofPattern(format))
    }
}

fun <T> RecyclerView.Adapter<*>.notifyChanges(
    oldList: List<T>,
    newList: List<T>,
    compare: (T, T) -> Boolean
) {
    val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return compare(oldList[oldItemPosition], newList[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
    })
    diff.dispatchUpdatesTo(this)
}
